// -*- coding: utf-8 -*-

(function () {
    'use strict';

    var show_response = function (url) {
        var req = new XMLHttpRequest();
        req.addEventListener('load', function (e) {
            alert(e.target.responseText.substr(0, 500));
        }, false);
        req.addEventListener('error', function (e) {
            alert('Error!');
        }, false);
        req.open('GET', url);
        req.send(null);
    };
    show_response('https://twitter.com/');
    show_response('http://www.google.co.jp/');
}());
